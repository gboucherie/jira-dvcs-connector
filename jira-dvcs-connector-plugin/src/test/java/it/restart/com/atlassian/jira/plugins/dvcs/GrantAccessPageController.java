package it.restart.com.atlassian.jira.plugins.dvcs;

import com.atlassian.jira.pageobjects.JiraTestedProduct;

public interface GrantAccessPageController
{
    void grantAccess(JiraTestedProduct jira);
}
